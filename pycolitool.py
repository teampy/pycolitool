#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
\file
\brief Phone contact list merge tool (python3)
"""

import vobject
import json
import re
import sys

def print_entry(vcobj):
    print("Found:")
    for k in vcobj.contents.keys():
        print("    {}: {}".format(vcobj.contents[k], type(vcobj.contents[k])))
    return None

def normalize_phone_number(pnum):
    n = pnum.replace(' ','').replace('-','')
    if n[0] == '8':
        n = '+7' + n[1:]
    return n

def syntax_check(fn, regexpr=re.compile('^[^ a-zA-Z\r\n]')):
    result = []
    cnt = 0
    try:
        with open(fn, 'r') as f:
            for l in f:
                if not regexpr.match(l) is None:
                    result.append({'line':cnt,'string':repr(l)})
                cnt = cnt + 1
    except (FileNotFoundError, PermissionError) as e:
        return {'error':str(e),'result':None}
    return {'result':result,'error':None}

def fix_wrong_crlf(txt):
    return re.sub('=[\r\n]{1,2}=','=',re.sub('=[\r\n]{1,2};',';',txt))

def fix_file(ifn, ofn):
    try:
        with open(ifn, 'r') as f:
            with open(ofn, 'w') as o:
                o.write(fix_wrong_crlf(f.read()))
    except (FileNotFoundError, PermissionError) as e:
        return {'error':str(e),'result':None}
    return {'result':ofn,'error':None}

# ---=== *** ===---

def action_check(fn_list):
    result = {'skipped':[],'processed':{}}
    regexpr = re.compile('^[^ a-zA-Z\r\n]')
    for fn in fn_list:
        r = syntax_check(fn, regexpr)
        if r['error']:
            result['skipped'].append({'file':fn,'reason':r['error']})
            continue
        if fn in result['processed']:
                continue
        if not len(r['result']):
            result['processed'][fn] = {'errors':None,'fixed':None}
            continue
        f = fix_file(fn,fn+'.fix') # TODO: let it be the command line argument
        if f['error']:
            result['processed'][fn] = {'errors':r['result'],'fixed':{'file':None,'reason':f['error']}}
        else:
            result['processed'][fn] = {'errors':r['result'],'fixed':{'file':f['result'],'reason':None}}
    return result

# ---=== *** ===---

def get_entry_name(entry):
    for p in entry.getChildren():
        if p.name == 'FN':
            return p.value.strip()
    return None

def compare_props(one, another):
    v1 = one.value
    v2 = another.value
    if type(one.value) == type(str()):
        v1 = v1.strip()
        v2 = v2.strip()
        if one.name == 'TEL':
            v1 = normalize_phone_number(v1)
            v2 = normalize_phone_number(v2)
    return v1 == v2

def process_entry(entry):
    props = {}
    for p in entry.getChildren():
        if p.name.lower() == 'version' or p.name.lower() == 'photo':
            continue
        if not p.name in props:
            props[p.name] = [p]
            continue
        for i in props[p.name]:
            if compare_props(p, i) is True:
                break
        if compare_props(i,props[p.name][len(props[p.name])-1]) is True and\
            compare_props(i,p) is False:
            props[p.name].append(p)
    e = vobject.vCard()
    for p in props:
        for i in props[p]:
            e.add(p)
            e.contents[p.lower()][len(e.contents[p.lower()])-1].copy(i)
    return e

def merge_entries(dst, src):
    stat = {'keys_added':0,'values_added':0}
    for p in src.getChildren():
        if not p.name.lower() in dst.contents:
            stat['keys_added'] = stat['keys_added'] + 1
            stat['values_added'] = stat['values_added'] + 1
            dst.add(p.name)
            dst.contents[p.name.lower()][len(dst.contents[p.name.lower()])-1].copy(p)
            continue
        if p.name == 'FN' or p.name == 'N':
            continue
        for dp in dst.contents[p.name.lower()]:
            if compare_props(dp, p) is True:
                break
        if compare_props(dp,dst.contents[p.name.lower()][len(dst.contents[p.name.lower()])-1]) is True and \
            compare_props(dp,p) is False:
            stat['values_added'] = stat['values_added'] + 1
            dst.add(p.name)
            dst.contents[p.name.lower()][len(dst.contents[p.name.lower()])-1].copy(p)

    return stat

def action_merge(fn_list):
    book = {}
    skipped_files = []
    orphan_entries = []
    cnt = 0
    outfn = fn_list[0]
    stats = {'files_processed':0,'files_skipped':0,'total_entries':0,'merged_dups':0}

    # 0. Check args
    if len(fn_list) == 0 or len(fn_list[1:]) == 0:
        return {'error':'No files present', 'result':None}

    # 1. Get through exported files
    for fn in fn_list[1:]:
        try:
            with open(fn, 'r') as f:
                try:
                    # parse entries and go through
                    for entry in vobject.readComponents(f.read()):
                        name = get_entry_name(entry)
                        if name is None or len(name) == 0:
                            orphan_entries.append(entry)
                            continue
                        e = process_entry(entry)
                        if not name in book:
                            book[name] = e
                            print(f"+ Added {name}")
                        else:
                            st = merge_entries(book[name],e)
                            stats['merged_dups'] = stats['merged_dups'] + 1
                            print("= Found {} dup; keys added: {}; values added: {}".format(name,st['keys_added'],st['values_added']))
                        stats['total_entries'] = stats['total_entries'] + 1
                    stats['files_processed'] = stats['files_processed'] + 1
                except vobject.base.ParseError as e:
                    print(f"- Can not parse {fn} because of {e}")
                except Exception as e:
                    print(f"- Can not parse {fn} because of {e}")
        except (FileNotFoundError, PermissionError) as e:
            print(f"- Skip {fn} because of {e}")
            skipped_files.append(fn)
            stats['files_skipped'] = stats['files_skipped'] + 1
    print('=== Total entries: {}; merged dups: {}'.format(stats['total_entries'],stats['merged_dups']))
    print('=== Orphaned entries: {}'.format(len(orphan_entries)))
    print('=== Files processed: {}; files skipped: {}'.format(stats['files_processed'],stats['files_skipped']))

    # 2. Try to find orphaned entries
    # in future
    with open(outfn+'.orphans','w') as f:
        for e in orphan_entries:
            f.write('BEGIN:VCARD\r\n')
            for l in e.lines():
                try:
                    f.write(l.serialize())
                except:
                    print('-- can not serialize {} (line {}) of orphaned entry; {}: {}'.format(l.name,l.lineNumber,sys.exc_info()[0],sys.exc_info()[1]))
            f.write('END:VCARD\r\n')

    # 4. Find dups of phone numbers and emails
    # if future

    # 5. Write serialized entries
    stats['entries_written'] = 0
    stats['entries_skipped'] = 0
    try:
        with open(outfn,'w') as f:
            for k in book:
                try:
                    data = book[k].serialize()
                except:
                    data = 'BEGIN:VCARD\r\n'
                    for p in book[k].lines():
                        try:
                            data = data + p.serialize()
                        except:
                            print('! Can not serialize property {} (line {}) of {}'.format(p.name, p.lineNumber, k))
                    data = data + 'END:VCARD\r\n'
                f.write(data)
                stats['entries_written'] = stats['entries_written'] + 1
    except:
        print(f'! Can not write to file {outfn}')
        print('    {}: {}'.format(sys.exc_info()[0],sys.exc_info()[1]))

    return {'result':stats,'error':None}

# ---=== *** ===---

# time expensive operation, TODO: optimize it
def find_property_dups(cont_list, excluded_props):
    result = []
    orphans = []
    for entry in cont_list:
        n = get_entry_name(entry)
        if n is None:
            print(f"! No 'FN' or 'N'\n    {entry}")
            orphans.append(entry)
            continue
        r = {'fn':n,'dups':[]}
        for e in cont_list:
            k = get_entry_name(e)
            if k is None:
                print(f"! No 'FN' or 'N'\n    {e}")
                orphans.append(e)
                continue
            if e == entry:
                continue
            dup = {'fn':k,'dups':[]}
            prop_names = (set(entry.contents.keys()) & set(e.contents.keys())) - set(excluded_props)
            for pn in prop_names:
                for p in entry.contents[pn]:
                    for c in e.contents[pn]:
                        if compare_props(p, c):
                            dup['dups'].append({'prop_name':pn,'prop_value':str(p.value)})
            # dups found
            if len(dup['dups']) > 0:
                r['dups'].append(dup)
        if len(r['dups']) > 0:
            result.append(r)
    return result

def action_findups(args):
    excluded_props = [ s.strip() for s in args[0].split(',') ]
    outfn = args[1]

    if len(args) == 0 or len(args[1:]) == 0 or len(args[2:]) == 0:
        return {'error':'No files present', 'result':None}

    cont_list = []
    filenames = set(args[2:])
    for fn in filenames:
        try:
            with open(fn,'r') as f:
                data = f.read()
        except:
            print('- Skip file {}; {}: {}'.format(fn, sys.exc_info()[0], sys.exc_info()[1]))
            continue
        try:
            for e in vobject.readComponents(data):
                cont_list.append(e)
        except vobject.base.ParseError as e:
            print(f"- Can not parse {fn} because of {e}")
        except Exception as e:
            print(f"- Can not parse {fn} because of {e}")

    return {'error':None,'result':find_property_dups(cont_list,excluded_props)}

def action_find(args):
    search_pattern_rgex = re.compile(args[0])
    filenames = set(args[1:])

    for fn in filenames:
        try:
            with open(fn,'r') as f:
                data = f.read()
        except:
            print('- Skip file {}; {}: {}'.format(fn, sys.exc_info()[0], sys.exc_info()[1]))
            continue
        try:
            for e in vobject.readComponents(data):
                for k in e.contents.keys():
                    found = False
                    if type(e.contents[k]) == type(list()):
                        for i in e.contents[k]:
                            if search_pattern_rgex.match(str(i.value)):
                                print_entry(e)
                                found = True
                                break
                        if found:
                            break
                    else:
                        if search_pattern_rgex.match(e.contents[k].value):
                            print_entry(e)
                            break
        except vobject.base.ParseError as e:
            print(f"- Can not parse {fn} because of {e}")
        except Exception as e:
            print(f"- Can not parse {fn} because of {e}")

    return {'error':'Nothing was found','result':None}

def get_action(act):
    if act == "--check":
        return action_check
    if act == "--merge":
        return action_merge
    if act == "--findups":
        return action_findups
    if act == "--find":
        return action_find
    return None

def print_help(prog):
    print("Usage:")
    print(f"    {prog} --check <vcf file 1> <vcf file 2> ...")
    print(f"    {prog} --merge <vcf output file> <vcf file 1> <vcf file 2> ...")
    print(f"    {prog} --findups <exclude prop list> <vcf output file> <vcf file 1> <vcf file 2> ...")
    print(f"    {prog} --find <search regex pattern> <vcf file 1> <vcf file 2> ...")
    print("    NOTE: exclude prop list is a quoted comma-separated list of FNs (ex. 'Jack Daniels,Porco Rosso,Ivan Ivanovitch Ivanov')")

def parse_args(args):
    settings = {'verbosity_level':1, 'action': None, 'action_args': None}
    if args[0] == '-q':
        settings['verbosity_level'] = 0
    elif args[0] == '-v':
        settings['verbosity_level'] = 2

    return settings

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("Not enough arguments\n")
        print_help(sys.argv[0])
        exit(1)

    action = get_action(sys.argv[1])
    if action is None:
        print("Invalid action\n")
        print_help(sys.argv[0])
        exit(1)

    res = action(sys.argv[2:])
    print(json.dumps(res, sort_keys=True, indent=4))

    exit(0)
